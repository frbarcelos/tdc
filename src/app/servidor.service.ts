import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, retryWhen } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class ServidorService {

  servidor = 'https://meumetret01.azurewebsites.net';
  // servidor = 'http://localhost:3000';

  constructor(private http: HttpClient) { }
  // Get
  busca(caminho: any, httpOptions?: any): any {
    const url = this.servidor + caminho;
    const observable = new Observable(observer => {
      this.http.get(url, httpOptions)
      .pipe(
        delayedRetry(1000, 3)
      )
      .subscribe(response => {
        observer.next(response);
      },
      () => {
        const resposta = {erro: 'Ocorreu um erro na requisição. Favor tentar novamente.'};
        observer.next(resposta);
      });
    });
    return observable;
  }
  // Post
  insere(caminho: any, dados: any, httpOptions?: any): any {
    const url = this.servidor + caminho;
    const observable = new Observable(observer => {
      this.http.post(url, dados, httpOptions)
      .pipe(
        delayedRetry(1000, 3)
      )
      .subscribe(response => {
        observer.next(response);
      },
      () => {
        const resposta = {erro: 'Ocorreu um erro na requisição. Favor tentar novamente.'};
        observer.next(resposta);
      });
    });
    return observable;
  }
}

export function delayedRetry(delayMS: number, maxRetray: number) {
  let retries = maxRetray;
  return ((src: Observable<any>) =>
    src.pipe(
      retryWhen((errors: Observable<any>) => errors.pipe (
        delay(delayMS),
        mergeMap(error => retries-- > 0 ? of(error) : throwError('Erro na requisição.'))
      ))
    )
  );
}
