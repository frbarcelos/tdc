import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import * as BusyIndicator from '@ui5/webcomponents/dist/BusyIndicator';
import * as FileSaver from 'file-saver';
import { VarglobalService } from '../varglobal.service';
import * as Buffer from 'buffer';

interface Problema {
  linha: number;
  campo: string;
  descricao: string;
  antes: string;
  depois: string;
}

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent implements OnInit, OnDestroy {

  @ViewChild('arquivo', { static: false }) arquivo;
  @ViewChild('busycontainer', { static: false }) busycontainer;

  arquivoSel;
  sText;
  txtName = '';
  txtCorrigido;

  problemas: Problema[] = [];

  mostrarProblemas = false;

  constructor(public varGlobal: VarglobalService) { }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.arquivoSel = undefined;
    this.txtCorrigido = undefined;
    this.txtName = '';
    this.problemas = [];
    this.mostrarProblemas = false;
  }

  abrirSelArquivo() {
    this.arquivo.nativeElement.click();
  }

  mudouArquivo(oEvent) {
    this.arquivoSel = oEvent.target.files[0] as File;
    if (this.arquivoSel !== undefined) {
      this.txtName = this.arquivoSel.name.substring(0, (this.arquivoSel.name.indexOf('.txt')));
      const oReader = new FileReader();
      oReader.onload = (() => {
        this.sText = oReader.result;
      });
      oReader.readAsText(this.arquivoSel);
      this.txtCorrigido = undefined;
      this.problemas = [];
      this.mostrarProblemas = false;
    } else {
      this.sText = undefined;
      if (!this.txtCorrigido) {
        this.txtName = '';
      }
    }
  }

  corrigir() {
    let i;
    const arrayTXT = this.sText.split('\n');
    let linhaAtual;
    let linhaCorrigida;
    let indOper;
    let codPart;
    let cnpjEmpresa;
    let qtdeLinhasBl0Adic = 0;
    let qtdeLinhasBl9Adic = 0;
    let total1100 = 0;
    let total1300 = 0;
    let total1500 = 0;
    let total1700 = 0;
    let temTotal1100 = false;
    let temTotal1300 = false;
    let temTotal1500 = false;
    let temTotal1700 = false;
    let sequencialC170 = 0;
    let linhaC100 = 0;
    let totalPisC100 = 0;
    let somaPisC170 = 0;
    let totalCofinsC100 = 0;
    let somaCofinsC170 = 0;
    let totalValorC100 = 0;
    let somaValorC170 = 0;
    let conta0100 = 0;
    let linhasRemovidas = 0;
    let ultimoC120 = '';
    let linhasCRemovidas = 0;
    this.problemas = [];
    for (i = 0; i < arrayTXT.length; i++) {
      // Para manter a acentuação e poder salvar como ANSI (latin1)
      arrayTXT[i] = JSON.parse(JSON.stringify(arrayTXT[i]));
      // Versão do layout
      if (arrayTXT[i].startsWith('|0000|')) {
        linhaAtual = arrayTXT[i].split('|');
        if (linhaAtual[2] === '003' || linhaAtual[2] === '004') {
          this.problemas.push({
            linha: i,
            campo: '|0000|',
            descricao: 'Versão do Layout errada',
            antes: linhaAtual[2],
            depois: '005'
          });
          linhaAtual[2] = '005';
          linhaCorrigida = linhaAtual.join('|');
          arrayTXT[i] = linhaCorrigida;
        }
        cnpjEmpresa = linhaAtual[9];
      }
      // Corrigindo registro 0100 repetido
      if (arrayTXT[i].startsWith('|0100|')) {
        conta0100++;
        if (conta0100 > 1) {
          this.problemas.push({
            linha: i + linhasRemovidas,
            campo: '|0100|',
            descricao: 'Duplicidade de ocorrência de chave de CPF',
            antes: 'Linha repetida',
            depois: ''
          });
          arrayTXT.splice(i, 1);
          i--;
          linhasRemovidas++;
        }
      }
      // Corrigindo registro 0140 da Matriz com IE '01505705000123 - CNPJ da OneSubsea' inválida (6º campo - IE)
      if (arrayTXT[i].startsWith('|0140|')) {
        linhaAtual = arrayTXT[i].split('|');
        if (linhaAtual[6] === '01505705000123') {
          this.problemas.push({
            linha: i,
            campo: '|0140|',
            descricao: 'IE da Matriz inválida',
            antes: '01505705000123',
            depois: ''
          });
          linhaAtual[6] = '';
          linhaCorrigida = linhaAtual.join('|');
          arrayTXT[i] = linhaCorrigida;
        }
      }
      // Corrigindo registro 0150 com IE '770597800273' inválida (6º campo - IE)
      if (arrayTXT[i].startsWith('|0150|')) {
        linhaAtual = arrayTXT[i].split('|');
        if (linhaAtual[7] === '770597800273') {
          this.problemas.push({
            linha: i,
            campo: '|0150|',
            descricao: 'IE inválida',
            antes: '770597800273',
            depois: ''
          });
          linhaAtual[7] = '';
          linhaCorrigida = linhaAtual.join('|');
          arrayTXT[i] = linhaCorrigida;
        }
      }
      // Corrigindo registros 0150 com os valores ISENTO ou ISENTA (7º campo - IE)
      if (arrayTXT[i].startsWith('|0150|')) {
        linhaAtual = arrayTXT[i].split('|');
        if (linhaAtual[7] === 'ISENTO' || linhaAtual[7] === 'ISENTA') {
          this.problemas.push({
            linha: i,
            campo: '|0150|',
            descricao: 'IE como ISENTO ou ISENTA',
            antes: linhaAtual[7],
            depois: ''
          });
          linhaAtual[7] = '';
          linhaCorrigida = linhaAtual.join('|');
          arrayTXT[i] = linhaCorrigida;
        }
      }
      // Corrigindo registro 0190 sem descrição da unidade de medida (3º DESCR)
      if (arrayTXT[i].startsWith('|0190|')) {
        linhaAtual = arrayTXT[i].split('|');
        if (linhaAtual[3] === '') {
          this.problemas.push({
            linha: i,
            campo: '|0190|',
            descricao: 'Unidade de medida sem descrição',
            antes: '',
            depois: 'BTO'
          });
          linhaAtual[3] = 'BTO';
          linhaCorrigida = linhaAtual.join('|');
          arrayTXT[i] = linhaCorrigida;
        }
      }
      // Corrigindo registros 0200 colocando 'PRODUTO SEM DESCRIÇÃO' nos produtos onde a descrição estiver vazia
      // (3º campo - DESCR_ITEM)
      if (arrayTXT[i].startsWith('|0200|')) {
        linhaAtual = arrayTXT[i].split('|');
        if (linhaAtual[3] === '') {
          this.problemas.push({
            linha: i,
            campo: '|0200|',
            descricao: 'Produto com o campo "Descrição" vazio',
            antes: '',
            depois: 'PRODUTO SEM DESCRIÇÃO'
          });
          linhaAtual[3] = 'PRODUTO SEM DESCRIÇÃO';
          linhaCorrigida = linhaAtual.join('|');
          arrayTXT[i] = linhaCorrigida;
        }
      }
      // Inserir códigos de contas contábeis (registro 0500) ao chegar no encerramento do bloco 0 (registro 0990)
      if (arrayTXT[i].startsWith('|0990|')) {
        linhaAtual = arrayTXT[i].split('|');
        const qtdeLinhasBl0 = parseInt(linhaAtual[2], 10);
        if (cnpjEmpresa === '07673377000150') {
          qtdeLinhasBl0Adic = 4;
          linhaAtual[2] = (qtdeLinhasBl0 + qtdeLinhasBl0Adic - linhasRemovidas).toString();
          linhaCorrigida = linhaAtual.join('|');
          arrayTXT[i] = linhaCorrigida;
          linhaAtual = '|0500|' + '01042017|' + '01|' + 'A|' + '0|' + '0000120000|' +
          'Trade Accounts Receivable|' + '|' + '|';
          arrayTXT.splice(i++, 0, linhaAtual);
          this.problemas.push({
            linha: null,
            campo: '|0500|',
            descricao: 'Inserção de código de conta contábil',
            antes: '',
            depois: linhaAtual
          });
          linhaAtual = '|0500|' + '01042017|' + '02|' + 'A|' + '0|' + '0000210000|' +
          'Trade Accounts Payable|' + '|' + '|';
          arrayTXT.splice(i++, 0, linhaAtual);
          this.problemas.push({
            linha: null,
            campo: '|0500|',
            descricao: 'Inserção de código de conta contábil',
            antes: '',
            depois: linhaAtual
          });
          linhaAtual = '|0500|' + '01042017|' + '01|' + 'A|' + '0|' + '0000300000|' +
          'Intercompany Accounts Receivable|' + '|' + '|';
          arrayTXT.splice(i++, 0, linhaAtual);
          this.problemas.push({
            linha: null,
            campo: '|0500|',
            descricao: 'Inserção de código de conta contábil',
            antes: '',
            depois: linhaAtual
          });
          linhaAtual = '|0500|' + '01042017|' + '02|' + 'A|' + '0|' + '0000310000|' +
          'Intercompany Accounts Payable|' + '|' + '|';
          arrayTXT.splice(i++, 0, linhaAtual);
          this.problemas.push({
            linha: null,
            campo: '|0500|',
            descricao: 'Inserção de código de conta contábil',
            antes: '',
            depois: linhaAtual
          });
        } else if (cnpjEmpresa === '01505705000123') {
          qtdeLinhasBl0Adic = 6;
          linhaAtual[2] = (qtdeLinhasBl0 + qtdeLinhasBl0Adic - linhasRemovidas).toString();
          linhaCorrigida = linhaAtual.join('|');
          arrayTXT[i] = linhaCorrigida;
          linhaAtual = '|0500|' + '01042017|' + '01|' + 'A|' + '0|' + '0000120000|' +
          'Trade Accounts Receivable|' + '|' + '|';
          arrayTXT.splice(i++, 0, linhaAtual);
          this.problemas.push({
            linha: null,
            campo: '|0500|',
            descricao: 'Inserção de código de conta contábil',
            antes: '',
            depois: linhaAtual
          });
          linhaAtual = '|0500|' + '01042017|' + '02|' + 'A|' + '0|' + '0000210000|' +
          'Trade Accounts Payable|' + '|' + '|';
          arrayTXT.splice(i++, 0, linhaAtual);
          this.problemas.push({
            linha: null,
            campo: '|0500|',
            descricao: 'Inserção de código de conta contábil',
            antes: '',
            depois: linhaAtual
          });
          linhaAtual = '|0500|' + '01042017|' + '01|' + 'A|' + '0|' + '0000300000|' +
          'Intercompany Accounts Receivable|' + '|' + '|';
          arrayTXT.splice(i++, 0, linhaAtual);
          this.problemas.push({
            linha: null,
            campo: '|0500|',
            descricao: 'Inserção de código de conta contábil',
            antes: '',
            depois: linhaAtual
          });
          linhaAtual = '|0500|' + '01042017|' + '02|' + 'A|' + '0|' + '0000310000|' +
          'Intercompany Accounts Payable|' + '|' + '|';
          arrayTXT.splice(i++, 0, linhaAtual);
          this.problemas.push({
            linha: null,
            campo: '|0500|',
            descricao: 'Inserção de código de conta contábil',
            antes: '',
            depois: linhaAtual
          });
          linhaAtual = '|0500|' + '01012019|' + '02|' + 'A|' + '0|' + '0000210020|' +
          'Conta 0000210020|' + '|' + '|';
          arrayTXT.splice(i++, 0, linhaAtual);
          this.problemas.push({
            linha: null,
            campo: '|0500|',
            descricao: 'Inserção de código de conta contábil',
            antes: '',
            depois: linhaAtual
          });
          linhaAtual = '|0500|' + '01122017|' + '09|' + 'A|' + '1|' + '9999999999|' +
          'CONTA 9999999999|' + '|' + '|';
          arrayTXT.splice(i++, 0, linhaAtual);
          this.problemas.push({
            linha: null,
            campo: '|0500|',
            descricao: 'Inserção de código de conta contábil',
            antes: '',
            depois: linhaAtual
          });
        } else if (cnpjEmpresa === '66659863000345') {
          qtdeLinhasBl0Adic = 3;
          linhaAtual[2] = (qtdeLinhasBl0 + qtdeLinhasBl0Adic - linhasRemovidas).toString();
          linhaCorrigida = linhaAtual.join('|');
          arrayTXT[i] = linhaCorrigida;
          linhaAtual = '|0500|' + '17052010|' + '01|' + 'A|' + '5|' + '0000120000|' +
          'Trade Accounts Receivable|' + '|' + '|';
          arrayTXT.splice(i++, 0, linhaAtual);
          this.problemas.push({
            linha: null,
            campo: '|0500|',
            descricao: 'Inserção de código de conta contábil',
            antes: '',
            depois: linhaAtual
          });
          linhaAtual = '|0500|' + '17052010|' + '02|' + 'A|' + '0|' + '0000210000|' +
          'Trade Accounts Payable|' + '|' + '|';
          arrayTXT.splice(i++, 0, linhaAtual);
          this.problemas.push({
            linha: null,
            campo: '|0500|',
            descricao: 'Inserção de código de conta contábil',
            antes: '',
            depois: linhaAtual
          });
          linhaAtual = '|0500|' + '17052010|' + '09|' + 'A|' + '1|' + '9999999999|' +
          'CONTA 9999999999|' + '|' + '|';
          arrayTXT.splice(i++, 0, linhaAtual);
          this.problemas.push({
            linha: null,
            campo: '|0500|',
            descricao: 'Inserção de código de conta contábil',
            antes: '',
            depois: linhaAtual
          });
        } else if (cnpjEmpresa === '21921393000146') {
          qtdeLinhasBl0Adic = 2;
          linhaAtual[2] = (qtdeLinhasBl0 + qtdeLinhasBl0Adic - linhasRemovidas).toString();
          linhaCorrigida = linhaAtual.join('|');
          arrayTXT[i] = linhaCorrigida;
          linhaAtual = '|0500|' + '17112015|' + '01|' + 'A|' + '5|' + '0000120000|' +
          'Trade Accounts Receivable|' + '|' + '|';
          arrayTXT.splice(i++, 0, linhaAtual);
          this.problemas.push({
            linha: null,
            campo: '|0500|',
            descricao: 'Inserção de código de conta contábil',
            antes: '',
            depois: linhaAtual
          });
          linhaAtual = '|0500|' + '17112015|' + '02|' + 'A|' + '0|' + '0000210000|' +
          'Trade Accounts Payable|' + '|' + '|';
          arrayTXT.splice(i++, 0, linhaAtual);
          this.problemas.push({
            linha: null,
            campo: '|0500|',
            descricao: 'Inserção de código de conta contábil',
            antes: '',
            depois: linhaAtual
          });
        }
        this.problemas.push({
          linha: i - qtdeLinhasBl0Adic,
          campo: '|0990|',
          descricao: 'Total de registros do bloco 0',
          antes: qtdeLinhasBl0.toString(),
          depois: (qtdeLinhasBl0 + qtdeLinhasBl0Adic - linhasRemovidas).toString()
        });
      }
      // Ler 2º campo - IND_OPER - (serviço contratado - 0 / serviço prestado - 1) dos registros A100
      if (arrayTXT[i].startsWith('|A100|')) {
        linhaAtual = arrayTXT[i].split('|');
        indOper = linhaAtual[2];
      }
      // Corrigindo registros A170 (Itens do Documento)
      if (arrayTXT[i].startsWith('|A170|')) {
        let alterouRegA170 = false;
        linhaAtual = arrayTXT[i].split('|');
        // Colocar '98' no CST PIS (9º campo - CST_PIS) quando estiver vazio ou '07'
        if (linhaAtual[9] === '' || linhaAtual[9] === '07') {
          this.problemas.push({
            linha: i - qtdeLinhasBl0Adic,
            campo: '|A170|',
            descricao: 'Campo CST_PIS vazio ou com valor "07"',
            antes: linhaAtual[9],
            depois: '98'
          });
          linhaAtual[9] = '98';
          alterouRegA170 = true;
        }
        // Colocar '98' no CST COFINS (13º campo - CST_COFINS) quando estiver vazio ou '07'
        if (linhaAtual[13] === '' || linhaAtual[13] === '07') {
          this.problemas.push({
            linha: i - qtdeLinhasBl0Adic,
            campo: '|A170|',
            descricao: 'Campo CST_COFINS vazio ou com valor "07"',
            antes: linhaAtual[13],
            depois: '98'
          });
          linhaAtual[13] = '98';
          alterouRegA170 = true;
        }
        // Colocar '98' ou '49' no CST PIS (9º campo) e no CST COFINS (13º campo) de acordo com a indOper se
        // o valor da base de cálculo do PIS/PASEP (10º campo - VL_BC_PIS) for vazio e
        // a alíquota do PIS/PASEP (11º campo - ALIQ_PIS) for vazia e
        // o valor do PIS/PASEP (12º campo - VL_PIS) for vazio
        if (linhaAtual[10] === '' && linhaAtual[11] === '' && linhaAtual[12] === '') {
          if (indOper === '0') {
            if (linhaAtual[9] !== '98') {
              this.problemas.push({
                linha: i - qtdeLinhasBl0Adic,
                campo: '|A170|',
                descricao: 'Colocar "98" no campo CST_PIS do serviço contratado sem valor do PIS/PASEP',
                antes: linhaAtual[9],
                depois: '98'
              });
              linhaAtual[9] = '98';
              alterouRegA170 = true;
            }
            if (linhaAtual[13] !== '98') {
              this.problemas.push({
                linha: i - qtdeLinhasBl0Adic,
                campo: '|A170|',
                descricao: 'Colocar "98" no campo CST_COFINS do serviço contratado sem valor do PIS/PASEP',
                antes: linhaAtual[13],
                depois: '98'
              });
              linhaAtual[13] = '98';
              alterouRegA170 = true;
            }
          } else {
            if (linhaAtual[9] !== '49') {
              this.problemas.push({
                linha: i - qtdeLinhasBl0Adic,
                campo: '|A170|',
                descricao: 'Colocar "49" no campo CST_PIS do serviço prestado sem valor do PIS/PASEP',
                antes: linhaAtual[9],
                depois: '49'
              });
              linhaAtual[9] = '49';
              alterouRegA170 = true;
            }
            if (linhaAtual[13] !== '49') {
              this.problemas.push({
                linha: i - qtdeLinhasBl0Adic,
                campo: '|A170|',
                descricao: 'Colocar "49" no campo CST_COFINS do serviço prestado sem valor do PIS/PASEP',
                antes: linhaAtual[9],
                depois: '49'
              });
              linhaAtual[13] = '49';
              alterouRegA170 = true;
            }
          }
        }
        // Ajustando o código da conta contábil (17º campo - COD_CTA) de acordo com a indOper
        if (linhaAtual[17] === '') {
          if (indOper === '0') {
            this.problemas.push({
              linha: i - qtdeLinhasBl0Adic,
              campo: '|A170|',
              descricao: 'Campo código da conta contábil vazio do serviço contratado',
              antes: '',
              depois: '9999999999'
            });
            linhaAtual[17] = '9999999999';
          } else {
            this.problemas.push({
              linha: i - qtdeLinhasBl0Adic,
              campo: '|A170|',
              descricao: 'Campo código da conta contábil vazio do serviço prestado',
              antes: '',
              depois: '1111111111'
            });
            linhaAtual[17] = '1111111111';
          }
          alterouRegA170 = true;
        }
        // Se teve alteração no registro
        if (alterouRegA170) {
          linhaCorrigida = linhaAtual.join('|');
          arrayTXT[i] = linhaCorrigida;
        }
      }
      // Ler 2º - IND_OPER (entrada - 0 / saída - 1) e 4º - COD_PART (ID emitente/adquirente) campos dos registros C100
      if (arrayTXT[i].startsWith('|C100|')) {
        let alterouRegC100 = false;
        const totalPis = Math.round((totalPisC100 + Number.EPSILON) * 100) / 100;
        const somaPis = Math.round((somaPisC170 + Number.EPSILON) * 100) / 100;
        const totalCofins = Math.round((totalCofinsC100 + Number.EPSILON) * 100) / 100;
        const somaCofins = Math.round((somaCofinsC170 + Number.EPSILON) * 100) / 100;
        const totalValor = Math.round((totalValorC100 + Number.EPSILON) * 100) / 100;
        const somaValor = Math.round((somaValorC170 + Number.EPSILON) * 100) / 100;
        if (totalPis < somaPis || totalCofins < somaCofins || totalValor < somaValor) {
          linhaAtual = arrayTXT[linhaC100].split('|');
          if (totalPis < somaPis) {
            this.problemas.push({
              linha: linhaC100 - qtdeLinhasBl0Adic,
              campo: '|C100|',
              descricao: 'Total do PIS do documento menor que o total do PIS da soma dos itens',
              antes: linhaAtual[26],
              depois: somaPisC170.toFixed(2).replace('.', ',')
            });
            linhaAtual[26] = somaPisC170.toFixed(2).replace('.', ',');
          }
          if (totalCofins < somaCofins) {
            this.problemas.push({
              linha: linhaC100 - qtdeLinhasBl0Adic,
              campo: '|C100|',
              descricao: 'Total do COFINS do documento menor que o total do COFINS da soma dos itens',
              antes: linhaAtual[27],
              depois: somaCofinsC170.toFixed(2).replace('.', ',')
            });
            linhaAtual[27] = somaCofinsC170.toFixed(2).replace('.', ',');
          }
          if (totalValor < somaValor) {
            this.problemas.push({
              linha: linhaC100 - qtdeLinhasBl0Adic,
              campo: '|C100|',
              descricao: 'Total do documento menor que o total da soma dos itens',
              antes: linhaAtual[16],
              depois: somaValorC170.toFixed(2).replace('.', ',')
            });
            linhaAtual[16] = somaValorC170.toFixed(2).replace('.', ',');
          }
          linhaCorrigida = linhaAtual.join('|');
          arrayTXT[linhaC100] = linhaCorrigida;
        }
        sequencialC170 = 0; // zerar a cada documento (utilizado se o número sequencial do item no documento estiver zerado)
        linhaC100 = i;
        linhaAtual = arrayTXT[i].split('|');
        totalPisC100 = parseFloat(linhaAtual[26].replace(',', '.'));
        somaPisC170 = 0;
        totalCofinsC100 = parseFloat(linhaAtual[27].replace(',', '.'));
        somaCofinsC170 = 0;
        totalValorC100 = parseFloat(linhaAtual[16].replace(',', '.'));
        somaValorC170 = 0;
        indOper = linhaAtual[2];
        codPart = linhaAtual[4];
        // Colocar '0' ou '1' no tipo de frete (17º campo - IND_FRT) quando estiver vazio (de acordo) com a indOper
        if (linhaAtual[17] === '') {
          if (indOper === '0') {
            this.problemas.push({
              linha: i - qtdeLinhasBl0Adic,
              campo: '|C100|',
              descricao: 'Campo tipo de frete vazio da entrada',
              antes: '',
              depois: '1'
            });
            linhaAtual[17] = '1';
          } else {
            this.problemas.push({
              linha: i - qtdeLinhasBl0Adic,
              campo: '|C100|',
              descricao: 'Campo tipo de frete vazio da saída',
              antes: '',
              depois: '0'
            });
            linhaAtual[17] = '0';
          }
          alterouRegC100 = true;
        }
        // Colocar '01' no documento fiscal (5º campo - COD_MOD) quando estiver com '00'
        if (linhaAtual[5] === '00') {
          this.problemas.push({
            linha: i - qtdeLinhasBl0Adic,
            campo: '|C100|',
            descricao: 'Campo código do modelo do documento fiscal com valor "00"',
            antes: '00',
            depois: '01'
          });
          linhaAtual[5] = '01';
          alterouRegC100 = true;
        }
        // Colocar '' no tipo de frete (17º campo - IND_FRT) quando documento estiver cancelado, denegado ou inutilizado
        if (linhaAtual[6] === '02' || linhaAtual[6] === '03' || linhaAtual[6] === '04' || linhaAtual[6] === '05') {
          if (linhaAtual[17] !== '') {
            this.problemas.push({
              linha: i - qtdeLinhasBl0Adic,
              campo: '|C100|',
              descricao: 'Informando tipo de frete para documento cancelado, denegado ou inutilizado',
              antes: linhaAtual[17],
              depois: ''
            });
            linhaAtual[17] = '';
            alterouRegC100 = true;
          }
        }
        // Se teve alteração no registro
        if (alterouRegC100) {
          linhaCorrigida = linhaAtual.join('|');
          arrayTXT[i] = linhaCorrigida;
        }
      }
      // Corrigindo registros C120 duplicados (importação)
      if (arrayTXT[i].startsWith('|C120|')) {
        linhaAtual = arrayTXT[i].split('|');
        if (linhaAtual[3] === ultimoC120) {
          this.problemas.push({
            linha: i + linhasCRemovidas,
            campo: '|C120|',
            descricao: 'Duplicidade de documentos de importação',
            antes: linhaAtual[3],
            depois: 'Linha Removida'
          });
          arrayTXT.splice(i, 1);
          i--;
          linhasCRemovidas++;
        } else {
          ultimoC120 = linhaAtual[3];
        }
      }
      // Corrigindo registros C170 (Itens do Documento)
      if (arrayTXT[i].startsWith('|C170|')) {
        let alterouRegC170 = false;
        linhaAtual = arrayTXT[i].split('|');
        somaPisC170 += parseFloat(linhaAtual[30].replace(',', '.'));
        somaCofinsC170 += parseFloat(linhaAtual[36].replace(',', '.'));
        somaValorC170 += parseFloat(linhaAtual[7].replace(',', '.'));
        // Colocar '98' no CST COFINS (31º campo - CST_COFINS) quando estiver vazio ou '49' (se o CST PIS for '98')
        if (linhaAtual[25] === '98') {
          if (linhaAtual[31] === '49' || linhaAtual[31] === '') {
            this.problemas.push({
              linha: i - qtdeLinhasBl0Adic,
              campo: '|C170|',
              descricao: 'Campo CST_COFINS vazio ou com valor "49" (CST_PIS com valor "98")',
              antes: linhaAtual[31],
              depois: '98'
            });
            linhaAtual[31] = '98';
            alterouRegC170 = true;
          }
        }
        // Colocar '98' ou '49' no CST PIS (25º campo) e no CST COFINS (31º campo) de acordo com a indOper se
        // o valor da base de cálculo do PIS/PASEP (26º campo - VL_BC_PIS) for vazio e
        // a quantidade da base de cáculo do PIS/PASEP (28º campo - QUANT_BC_PIS) for vazia e
        // o valor do PIS/PASEP (30º campo - VL_PIS) for vazio
        if (linhaAtual[26] === '' && linhaAtual[28] === '' && linhaAtual[30] === '') {
          if (indOper === '0') {
            if (linhaAtual[25] !== '98') {
              this.problemas.push({
                linha: i - qtdeLinhasBl0Adic,
                campo: '|C170|',
                descricao: 'Colocar "98" no campo CST_PIS da entrada sem valor do PIS/PASEP',
                antes: linhaAtual[25],
                depois: '98'
              });
              linhaAtual[25] = '98';
              alterouRegC170 = true;
            }
            if (linhaAtual[31] !== '98') {
              this.problemas.push({
                linha: i - qtdeLinhasBl0Adic,
                campo: '|C170|',
                descricao: 'Colocar "98" no campo CST_COFINS da entrada sem valor do PIS/PASEP',
                antes: linhaAtual[31],
                depois: '98'
              });
              linhaAtual[31] = '98';
              alterouRegC170 = true;
            }
          } else {
            if (linhaAtual[25] !== '49') {
              this.problemas.push({
                linha: i - qtdeLinhasBl0Adic,
                campo: '|C170|',
                descricao: 'Colocar "49" no campo CST_PIS da saída sem valor do PIS/PASEP',
                antes: linhaAtual[25],
                depois: '49'
              });
              linhaAtual[25] = '49';
              alterouRegC170 = true;
            }
            if (linhaAtual[31] !== '49') {
              this.problemas.push({
                linha: i - qtdeLinhasBl0Adic,
                campo: '|C170|',
                descricao: 'Colocar "49" no campo CST_COFINS da saída sem valor do PIS/PASEP',
                antes: linhaAtual[31],
                depois: '49'
              });
              linhaAtual[31] = '49';
              alterouRegC170 = true;
            }
          }
        }
        // O CST PIS/PASEP (25º campo - CST_PIS) não pode ser 01 quando for entrada
        if (indOper === '0') {
          if (linhaAtual[25] === '01') {
            this.problemas.push({
              linha: i - qtdeLinhasBl0Adic,
              campo: '|C170|',
              descricao: 'O CST PIS não pode ser 01 quando for uma entrada',
              antes: '01',
              depois: '98'
            });
            linhaAtual[25] = '98';
          }
          if (linhaAtual[31] === '01') {
            this.problemas.push({
              linha: i - qtdeLinhasBl0Adic,
              campo: '|C170|',
              descricao: 'O CST COFINS não pode ser 01 quando for uma entrada',
              antes: '01',
              depois: '98'
            });
            linhaAtual[25] = '98';
          }
          alterouRegC170 = true;
        }
        // Colocar o CST COFINS (31º campo - CST_COFINS) igual ao CST PIS.
        if (linhaAtual[31] !== linhaAtual[25]) {
          this.problemas.push({
            linha: i - qtdeLinhasBl0Adic,
            campo: '|C170|',
            descricao: 'Campo CST_COFINS diferente do CST_PIS',
            antes: linhaAtual[31],
            depois: linhaAtual[25]
          });
          linhaAtual[31] = linhaAtual[25];
          alterouRegC170 = true;
        }
        // Ajustando a alíquota do PIS (27º campo - ALIQ_PIS) quando zerada
        if (linhaAtual[27] === '0') {
          this.problemas.push({
            linha: i - qtdeLinhasBl0Adic,
            campo: '|C170|',
            descricao: 'Alíquota do PIS zerada',
            antes: linhaAtual[27],
            depois: '1,6500'
          });
          linhaAtual[27] = '1,6500';
          alterouRegC170 = true;
        }
        // Ajustando a alíquota do COFINS (33º campo - ALIQ_COFINS) quando zerada
        if (linhaAtual[33] === '0') {
          this.problemas.push({
            linha: i - qtdeLinhasBl0Adic,
            campo: '|C170|',
            descricao: 'Alíquota do COFINS zerada',
            antes: linhaAtual[33],
            depois: '7,6000'
          });
          linhaAtual[33] = '7,6000';
          alterouRegC170 = true;
        }
        // Ajustando a alíquota do COFINS (33º campo - ALIQ_COFINS)
        if (linhaAtual[33] === '10,6500' || linhaAtual[33] === '10,65') {
          this.problemas.push({
            linha: i - qtdeLinhasBl0Adic,
            campo: '|C170|',
            descricao: 'Alíquota do COFINS errada',
            antes: linhaAtual[33],
            depois: '9,6500'
          });
          linhaAtual[33] = '9,6500';
          alterouRegC170 = true;
        }
        // Colocar '0000310000' ou '0000210000' no código da conta contábil (37º campo - COD_CTA)
        // de acordo com a indOper e o codPart
        if (linhaAtual[37] !== '0000210000' && linhaAtual[37] !== '0000120000' &&
        linhaAtual[37] !== '0000310000' && linhaAtual[37] !== '0000300000') {
          if (codPart === 'VVA714' || codPart === 'CCA539' || codPart === 'VVA134' || codPart === 'VVA537' ||
          codPart === 'VVA362' || codPart === 'VVA347' || codPart === 'VVA530' || codPart === 'VVA352' ||
          codPart === 'VVA521' || codPart === 'VVA506' || codPart === 'CSBPA714' || codPart === 'VVA715' ||
          codPart === 'VVA705' || codPart === 'VVA451' || codPart === 'VVA403' || codPart === 'VVA470' ||
          codPart === 'VVA703' || codPart === 'VVA526' || codPart === 'CCA521' || codPart === 'VVA428' ||
          codPart === 'VVA707' || codPart === 'VVA453' || codPart === 'CC0159200' || codPart === 'CCA714' ||
          codPart === 'VVA326' || codPart === 'CCSA526' || codPart === 'VVA717' || codPart === 'VVA716' ||
          codPart === 'VVA701' || codPart === 'VVA351' || codPart === 'VVA712' || codPart === 'VVA539' ||
          codPart === 'CCA532' || codPart === 'VVA402' || codPart === 'VVA538' || codPart === 'CCA114' ||
          codPart === 'CCA703' || codPart === 'VVA738' || codPart === 'CCA530' || codPart === 'CCA217' ||
          codPart === 'CCA420' || codPart === 'CCA409' || codPart === 'CCA408' || codPart === 'CCSA539' ||
          codPart === 'C0023216062' || codPart === 'CCA434' || codPart === 'VVA311' || codPart === 'VVA704' ||
          codPart === 'VVA463'  || codPart === 'VVA515' || codPart === 'VV0144200' || codPart === 'CCA470' ||
          codPart === 'V0010162205') {
            if (indOper === '0') {
              this.problemas.push({
                linha: i - qtdeLinhasBl0Adic,
                campo: '|C170|',
                descricao: 'Código da Conta Contábil na entrada por transferência',
                antes: linhaAtual[37],
                depois: '0000310000'
              });
              linhaAtual[37] = '0000310000';
            } else {
              this.problemas.push({
                linha: i - qtdeLinhasBl0Adic,
                campo: '|C170|',
                descricao: 'Código da Conta Contábil na saída para transferência',
                antes: linhaAtual[37],
                depois: '0000300000'
              });
              linhaAtual[37] = '0000300000';
            }
          } else {
            // eslint-disable-next-line no-lonely-if
            if (indOper === '0') {
              this.problemas.push({
                linha: i - qtdeLinhasBl0Adic,
                campo: '|C170|',
                descricao: 'Código da Conta Contábil na entrada',
                antes: linhaAtual[37],
                depois: '0000210000'
              });
              linhaAtual[37] = '0000210000';
            } else {
              this.problemas.push({
                linha: i - qtdeLinhasBl0Adic,
                campo: '|C170|',
                descricao: 'Código da Conta Contábil na saída',
                antes: linhaAtual[37],
                depois: '0000120000'
              });
              linhaAtual[37] = '0000120000';
            }
          }
          alterouRegC170 = true;
        }
        // Colocando a base de cálculo do COFINS (32º campo - VL_BC_COFINS) igual à base de cálculo do PIS (26)
        if (linhaAtual[32] !== linhaAtual[26]) {
          this.problemas.push({
            linha: i - qtdeLinhasBl0Adic,
            campo: '|C170|',
            descricao: 'Base de cálculo do COFINS diferente da do PIS',
            antes: linhaAtual[32],
            depois: linhaAtual[26]
          });
          linhaAtual[32] = linhaAtual[26];
          alterouRegC170 = true;
        }
        // Colocando o número sequencial (2º campo - NUM_ITEM) quando o mesmo está zerado
        if (linhaAtual[2] === '000') {
          sequencialC170++;
          const sequencial = '000'.substr((sequencialC170).toString().length, 3) + (sequencialC170).toString();
          this.problemas.push({
            linha: i - qtdeLinhasBl0Adic,
            campo: '|C170|',
            descricao: 'Número sequencial igual a 000',
            antes: '000',
            depois: sequencial
          });
          linhaAtual[2] = sequencial;
          alterouRegC170 = true;
        } else {
          sequencialC170 = parseInt(linhaAtual[2], 10);
        }
        // Ajustando a alíquota do PIS (27º campo - ALIQ_PIS) quando CST = 01
        if (linhaAtual[25] === '01') {
          if (linhaAtual[27] !== '1,65') {
            this.problemas.push({
              linha: i - qtdeLinhasBl0Adic,
              campo: '|C170|',
              descricao: 'Ajustando a alíquota do PIS quando CST = 01',
              antes: linhaAtual[27],
              depois: '1,65'
            });
            linhaAtual[27] = '1,65';
            alterouRegC170 = true;
          }
        }
        // Ajustando a alíquota do COFINS (33º campo - ALIQ_COFINS) quando CST = 01
        if (linhaAtual[31] === '01') {
          if (linhaAtual[33] !== '7,60') {
            this.problemas.push({
              linha: i - qtdeLinhasBl0Adic,
              campo: '|C170|',
              descricao: 'Ajustando a alíquota do COFINS quando CST = 01',
              antes: linhaAtual[33],
              depois: '1,65'
            });
            linhaAtual[33] = '7,60';
            alterouRegC170 = true;
          }
        }
        // Se teve alteração no registro
        if (alterouRegC170) {
          linhaCorrigida = linhaAtual.join('|');
          arrayTXT[i] = linhaCorrigida;
        }
      }
      // Totalizador dos campos C
      if (arrayTXT[i].startsWith('|C990|')) {
        // Se os valores errados estiverem no último registro C100
        const totalPis = Math.round((totalPisC100 + Number.EPSILON) * 100) / 100;
        const somaPis = Math.round((somaPisC170 + Number.EPSILON) * 100) / 100;
        const totalCofins = Math.round((totalCofinsC100 + Number.EPSILON) * 100) / 100;
        const somaCofins = Math.round((somaCofinsC170 + Number.EPSILON) * 100) / 100;
        const totalValor = Math.round((totalValorC100 + Number.EPSILON) * 100) / 100;
        const somaValor = Math.round((somaValorC170 + Number.EPSILON) * 100) / 100;
        if (totalPis < somaPis || totalCofins < somaCofins || totalValor < somaValor) {
          linhaAtual = arrayTXT[linhaC100].split('|');
          if (totalPis < somaPis) {
            this.problemas.push({
              linha: linhaC100 - qtdeLinhasBl0Adic,
              campo: '|C100|',
              descricao: 'Total do PIS do documento menor que o total do PIS da soma dos itens',
              antes: linhaAtual[26],
              depois: somaPisC170.toFixed(2).replace('.', ',')
            });
            linhaAtual[26] = somaPisC170.toFixed(2).replace('.', ',');
          }
          if (totalCofins < somaCofins) {
            this.problemas.push({
              linha: linhaC100 - qtdeLinhasBl0Adic,
              campo: '|C100|',
              descricao: 'Total do COFINS do documento menor que o total do COFINS da soma dos itens',
              antes: linhaAtual[27],
              depois: somaCofinsC170.toFixed(2).replace('.', ',')
            });
            linhaAtual[27] = somaCofinsC170.toFixed(2).replace('.', ',');
          }
          if (totalValor < somaValor) {
            this.problemas.push({
              linha: linhaC100 - qtdeLinhasBl0Adic,
              campo: '|C100|',
              descricao: 'Total do documento menor que o total da soma dos itens',
              antes: linhaAtual[16],
              depois: somaValorC170.toFixed(2).replace('.', ',')
            });
            linhaAtual[16] = somaValorC170.toFixed(2).replace('.', ',');
          }
          linhaCorrigida = linhaAtual.join('|');
          arrayTXT[linhaC100] = linhaCorrigida;
        }
        // Se foi excluído algum campo C
        if (linhasCRemovidas > 0) {
          linhaAtual = arrayTXT[i].split('|');
          const qtdeLinhasBlC = parseInt(linhaAtual[2], 10);
          this.problemas.push({
            linha: i + linhasCRemovidas,
            campo: '|C990|',
            descricao: 'Total de registros do bloco C',
            antes: linhaAtual[2],
            depois: (qtdeLinhasBlC - linhasCRemovidas).toString()
          });
          linhaAtual[2] = (qtdeLinhasBlC - linhasCRemovidas).toString();
          linhaCorrigida = linhaAtual.join('|');
          arrayTXT[i] = linhaCorrigida;
        }
      }
      // Corrigindo registros D100 (Aquisição de Serviços de Transporte)
      if (arrayTXT[i].startsWith('|D100|')) {
        let alterouRegD100 = false;
        linhaAtual = arrayTXT[i].split('|');
        // Colocar '1' no indicador do tipo do frete (17º campo - IND_FRT) quando estiver vazio
        if (linhaAtual[17] === '') {
          this.problemas.push({
            linha: i - qtdeLinhasBl0Adic,
            campo: '|D100|',
            descricao: 'Indicador do Tipo do Frete vazio',
            antes: '',
            depois: '1'
          });
          linhaAtual[17] = '1';
          alterouRegD100 = true;
        }
        // Ajustando o código da conta contábil (23º campo - COD_CTA) - D100 é sempre aquisição
        if (linhaAtual[23] === '') {
          this.problemas.push({
            linha: i - qtdeLinhasBl0Adic,
            campo: '|D100|',
            descricao: 'Código da Conta Contábil vazio',
            antes: '',
            depois: '9999999999'
          });
          linhaAtual[23] = '9999999999';
          alterouRegD100 = true;
        } else if (linhaAtual[23] === '0000210020') {
          this.problemas.push({
            linha: i - qtdeLinhasBl0Adic,
            campo: '|D100|',
            descricao: 'Código da Conta Contábil com o valor "0000210020"',
            antes: '0000210020',
            depois: '0000210000'
          });
          linhaAtual[23] = '0000210000';
          alterouRegD100 = true;
        }
        // Se teve alteração no registro
        if (alterouRegD100) {
          linhaCorrigida = linhaAtual.join('|');
          arrayTXT[i] = linhaCorrigida;
        }
      }
      // Corrigindo registros D101 (Complemento do Documento de Transporte - PIS/Pasep)
      // campo Indicador da Natureza do Frete (2º campo - IND_NAT_FRT)
      if (arrayTXT[i].startsWith('|D101|')) {
        let alterouRegD101 = false;
        linhaAtual = arrayTXT[i].split('|');
        if (linhaAtual[2] === '') {
          this.problemas.push({
            linha: i - qtdeLinhasBl0Adic,
            campo: '|D101|',
            descricao: 'Indicador da Natureza do Frete vazio',
            antes: '',
            depois: '9'
          });
          linhaAtual[2] = '9';
          alterouRegD101 = true;
        }
        if (linhaAtual[9] === '') {
          this.problemas.push({
            linha: i - qtdeLinhasBl0Adic,
            campo: '|D101|',
            descricao: 'Código da Conta Contábil vazio',
            antes: '',
            depois: '9999999999'
          });
          linhaAtual[9] = '9999999999';
          alterouRegD101 = true;
        }
        if (alterouRegD101) {
          linhaCorrigida = linhaAtual.join('|');
          arrayTXT[i] = linhaCorrigida;
        }
      }
      // Corrigindo registros D105 (Complemento do Documento de Transporte - Cofins)
      // campo Indicador da Natureza do Frete (2º campo - IND_NAT_FRT)
      if (arrayTXT[i].startsWith('|D105|')) {
        let alterouRegD105 = false;
        linhaAtual = arrayTXT[i].split('|');
        if (linhaAtual[2] === '') {
          this.problemas.push({
            linha: i - qtdeLinhasBl0Adic,
            campo: '|D105|',
            descricao: 'Indicador da Natureza do Frete vazio',
            antes: '',
            depois: '9'
          });
          linhaAtual[2] = '9';
          alterouRegD105 = true;
        }
        if (linhaAtual[9] === '') {
          this.problemas.push({
            linha: i - qtdeLinhasBl0Adic,
            campo: '|D105|',
            descricao: 'Código da Conta Contábil vazio',
            antes: '',
            depois: '9999999999'
          });
          linhaAtual[9] = '9999999999';
          alterouRegD105 = true;
        }
        if (alterouRegD105) {
          linhaCorrigida = linhaAtual.join('|');
          arrayTXT[i] = linhaCorrigida;
        }
      }
      // Contar registros 1100
      if (arrayTXT[i].startsWith('|1100|')) {
        total1100++;
      }
      // Contar registros 1300
      if (arrayTXT[i].startsWith('|1300|')) {
        total1300++;
      }
      // Contar registros 1500
      if (arrayTXT[i].startsWith('|1500|')) {
        total1500++;
      }
      // Contar registros 1700
      if (arrayTXT[i].startsWith('|1700|')) {
        total1700++;
      }
      // Verificar se há totalizadores (e já coloca o calculado se houver diferença) para os registros
      // 1100, 1300, 1500 e 1700 (registro 9900)
      if (arrayTXT[i].startsWith('|9900|')) {
        linhaAtual = arrayTXT[i].split('|');
        if (linhaAtual[2] === '1100') {
          temTotal1100 = true;
          if (linhaAtual[3] !== total1100.toString()) {
            this.problemas.push({
              linha: i - qtdeLinhasBl0Adic,
              campo: '|9900|',
              descricao: 'Totalizador para o registro 1100',
              antes: linhaAtual[3],
              depois: total1100.toString()
            });
            linhaAtual[3] = total1100;
            linhaCorrigida = linhaAtual.join('|');
            arrayTXT[i] = linhaCorrigida;
          }
        }
        if (linhaAtual[2] === '1300') {
          temTotal1300 = true;
          if (linhaAtual[3] !== total1300.toString()) {
            this.problemas.push({
              linha: i - qtdeLinhasBl0Adic,
              campo: '|9900|',
              descricao: 'Totalizador para o registro 1300',
              antes: linhaAtual[3],
              depois: total1300.toString()
            });
            linhaAtual[3] = total1300;
            linhaCorrigida = linhaAtual.join('|');
            arrayTXT[i] = linhaCorrigida;
          }
        }
        if (linhaAtual[2] === '1500') {
          temTotal1500 = true;
          if (linhaAtual[3] !== total1500.toString()) {
            this.problemas.push({
              linha: i - qtdeLinhasBl0Adic,
              campo: '|9900|',
              descricao: 'Totalizador para o registro 1500',
              antes: linhaAtual[3],
              depois: total1500.toString()
            });
            linhaAtual[3] = total1500;
            linhaCorrigida = linhaAtual.join('|');
            arrayTXT[i] = linhaCorrigida;
          }
        }
        if (linhaAtual[2] === '1700') {
          temTotal1700 = true;
          if (linhaAtual[3] !== total1700.toString()) {
            this.problemas.push({
              linha: i - qtdeLinhasBl0Adic,
              campo: '|9900|',
              descricao: 'Totalizador para o registro 1700',
              antes: linhaAtual[3],
              depois: total1700.toString()
            });
            linhaAtual[3] = total1700;
            linhaCorrigida = linhaAtual.join('|');
            arrayTXT[i] = linhaCorrigida;
          }
        }
      }
      // Atualizar totalizadores e total de totalizadores (registro 9990)
      if (arrayTXT[i].startsWith('|9990|')) {
        if (total1100 > 0 && !temTotal1100) {
          qtdeLinhasBl9Adic++;
          linhaAtual = '|9900|' + '1100|' + total1100.toString() + '|';
          arrayTXT.splice(i++, 0, linhaAtual);
          this.problemas.push({
            linha: null,
            campo: '|9990|',
            descricao: 'Inserção de totalizador dos registros 1100',
            antes: '',
            depois: linhaAtual
          });
        }
        if (total1300 > 0 && !temTotal1300) {
          qtdeLinhasBl9Adic++;
          linhaAtual = '|9900|' + '1300|' + total1300.toString() + '|';
          arrayTXT.splice(i++, 0, linhaAtual);
          this.problemas.push({
            linha: null,
            campo: '|9990|',
            descricao: 'Inserção de totalizador dos registros 1300',
            antes: '',
            depois: linhaAtual
          });
        }
        if (total1500 > 0 && !temTotal1500) {
          qtdeLinhasBl9Adic++;
          linhaAtual = '|9900|' + '1500|' + total1500.toString() + '|';
          arrayTXT.splice(i++, 0, linhaAtual);
          this.problemas.push({
            linha: null,
            campo: '|9990|',
            descricao: 'Inserção de totalizador dos registros 1500',
            antes: '',
            depois: linhaAtual
          });
        }
        if (total1700 > 0 && !temTotal1700) {
          qtdeLinhasBl9Adic++;
          linhaAtual = '|9900|' + '1700|' + total1700.toString() + '|';
          arrayTXT.splice(i++, 0, linhaAtual);
          this.problemas.push({
            linha: null,
            campo: '|9990|',
            descricao: 'Inserção de totalizador dos registros 1700',
            antes: '',
            depois: linhaAtual
          });
        }
        if (qtdeLinhasBl9Adic > 0) {
          linhaAtual = arrayTXT[i].split('|');
          const qtdeLinhasTotalizadores = parseInt(linhaAtual[2], 10);
          this.problemas.push({
            linha: i - qtdeLinhasBl0Adic - qtdeLinhasBl9Adic,
            campo: '|9990|',
            descricao: 'Total de totalizadores',
            antes: linhaAtual[2],
            depois: (qtdeLinhasTotalizadores + qtdeLinhasBl9Adic).toString()
          });
          linhaAtual[2] = (qtdeLinhasTotalizadores + qtdeLinhasBl9Adic).toString();
          linhaCorrigida = linhaAtual.join('|');
          arrayTXT[i] = linhaCorrigida;
        }
      }
      // Inserir os totais de linhas adicionadas ao chegar no encerramento do arquivo (registro 9999)
      // - Códigos de contas contábeis (registro 0500);
      // - Totalizadores (registro 9900);
      if (arrayTXT[i].startsWith('|9999|')) {
        linhaAtual = arrayTXT[i].split('|');
        // let qtdeLinhasTotal = parseInt(linhaAtual[2], 10);
        if (linhaAtual[2] !== (i + 1)) {
          this.problemas.push({
            linha: i - qtdeLinhasBl0Adic - qtdeLinhasBl9Adic,
            campo: '|9999|',
            descricao: 'Total geral de registros',
            antes: linhaAtual[2],
            depois: (i + 1).toString()
          });
          linhaAtual[2] = i + 1; // (qtdeLinhasTotal + qtdeLinhasBl0Adic + qtdeLinhasBl9Adic).toString();
          linhaCorrigida = linhaAtual.join('|');
          arrayTXT[i] = linhaCorrigida;
        }
      }
    }
    this.txtCorrigido = arrayTXT.join('\n');
  }

  listarProblemas() {
    this.busycontainer.nativeElement.style.visibility = 'visible';
    this.busycontainer.nativeElement.style.marginTop = '50px';
    setTimeout(() => {
      this.mostrarProblemas = true;
    }, 50);
  }

  carregouProblemas() {
    this.busycontainer.nativeElement.style.visibility = 'hidden';
    this.busycontainer.nativeElement.style.marginTop = '0';
  }

  salvarArquivo() {
    const buff = Buffer.Buffer.from(this.txtCorrigido, 'latin1');
    const blob = new Blob([buff], {type: 'text/plain'});
    FileSaver.saveAs(blob, this.txtName + ' - Corrigido' + '.txt');
  }

  exportaProblemasCSV() {
    const replacer = (key, value) => value === null ? '' : value; // specify how you want to handle null values here
    const header = Object.keys(this.problemas[0]);
    const csv = this.problemas.map(row => header.map(fieldName => JSON.stringify(row[fieldName], replacer)).join(','));
    csv.unshift(header.join(','));
    const csvArray = csv.join('\r\n');

    const buff = Buffer.Buffer.from(csvArray, 'latin1');
    const blob = new Blob([buff], {type: 'text/csv' });
    saveAs(blob, 'correcoes.csv');
  }
}
