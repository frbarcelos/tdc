import { TestBed } from '@angular/core/testing';

import { VarglobalService } from './varglobal.service';

describe('VarglobalService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VarglobalService = TestBed.get(VarglobalService);
    expect(service).toBeTruthy();
  });
});
