import { Injectable } from '@angular/core';
import { ServidorService } from './servidor.service';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  constructor(private servidor: ServidorService) { }

  // Verifica se usuário e a senha conferem
  buscaUsuario(usuario: any, senha: any): any {
    const url = '/loginsaptc/' + usuario + '/' + senha;
    return this.servidor.busca(url);
  }
}
