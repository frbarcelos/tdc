import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class VarglobalService {

  logado: boolean = (sessionStorage.getItem('logado') === 'true');

  constructor() { }
}
