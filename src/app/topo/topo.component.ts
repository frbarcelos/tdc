import { Component, OnInit } from '@angular/core';
import { VarglobalService } from '../varglobal.service';
import { UsuariosService } from '../usuarios.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-topo',
  templateUrl: './topo.component.html',
  styleUrls: ['./topo.component.css']
})
export class TopoComponent implements OnInit {

  usuario = '';
  senha = '';
  mensagemDialogo = '';
  shellbar;
  popMenu;
  menuAberto = false;

  constructor(public varGlobal: VarglobalService, private router: Router, private usuariosSrv: UsuariosService) { }

  ngOnInit() {
    this.shellbar = document.getElementById('shellbar') as any;
    this.popMenu = document.getElementById('menu') as any;
    this.shellbar.addEventListener('productSwitchClick', ((event) => {
      if (this.menuAberto) {
        this.menuAberto = false;
      } else {
        this.popMenu.openBy(event.detail.targetRef);
      }
    }));
    this.popMenu.addEventListener('afterClose', (() => {
      setTimeout(() => {
        this.menuAberto = false;
      }, 300);
    }));
    this.popMenu.addEventListener('afterOpen', (() => {
      this.menuAberto = true;
    }));
    if (this.varGlobal.logado) {
      this.shellbar.profile = '../../assets/profile-white.png';
      this.shellbar.showProductSwitch = true;
    } else {
      this.shellbar.profile = '../../assets/profile.png';
      this.shellbar.showProductSwitch = false;
    }
  }

  logar() {
    const cxDialogo = document.getElementById('cxdialogo') as any;
    if (this.usuario !== '') {
      if (this.senha !== '') {
        document.body.style.cursor = 'wait';
        this.usuariosSrv.buscaUsuario(this.usuario, this.senha)
        .subscribe((resposta) => {
          document.body.style.cursor = 'default';
          if (resposta.erro === undefined) {
            if (resposta.encontrado) {
              if (resposta.senhaOk) {
                const pop = document.getElementById('popover') as any;
                pop.close();
                this.shellbar.profile = '../../assets/profile-white.png';
                this.shellbar.showProductSwitch = true;
                this.varGlobal.logado = true;
                sessionStorage.setItem('logado', 'true');
                this.usuario = '';
                this.senha = '';
              } else {
                cxDialogo.headerText = 'Senha';
                this.mensagemDialogo = 'A senha informada não confere. Favor verificar.';
                cxDialogo.open();
              }
            } else {
              cxDialogo.headerText = 'Usuário';
              this.mensagemDialogo = 'Usuário não encontrado. Favor verificar.';
              cxDialogo.open();
            }
          }
        });
      } else {
        cxDialogo.headerText = 'Senha';
        this.mensagemDialogo = 'Favor informar a senha de usuário.';
        cxDialogo.open();
      }
    } else {
      cxDialogo.headerText = 'Usuário';
      this.mensagemDialogo = 'Favor informar o nome de usuário.';
      cxDialogo.open();
    }
  }

  sair() {
    const pop = document.getElementById('popover') as any;
    pop.close();
    this.shellbar.showProductSwitch = false;
    this.varGlobal.logado = false;
    this.shellbar.profile = '../../assets/profile.png';
    this.router.navigate(['home']);
    sessionStorage.removeItem('logado');
  }

  fecharDialogo() {
    const cxDialogo = document.getElementById('cxdialogo') as any;
    cxDialogo.close();
  }

  paraHome() {
    this.router.navigate(['home']);
    this.popMenu.close();
  }

  paraContrib() {
    this.router.navigate(['principal']);
    this.popMenu.close();
  }

}
